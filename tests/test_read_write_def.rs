#![cfg(test)]

// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use libreda_lefdef::{def_parser, def_writer};

#[test]
fn test_read_write_def() {
    // Simple DEF file.
    let data = r#"
VERSION 5.7 ;
DIVIDERCHAR "|" ;
BUSBITCHARS "<>" ;
DESIGN test_design ;
UNITS DISTANCE MICRONS 2000 ;
TECHNOLOGY FreePDK45 ;

HISTORY This is a test. ;
HISTORY This is an other test. ;


PROPERTYDEFINITIONS
    COMPONENTPIN designRuleWidth REAL ;
    DESIGN testProperty REAL 0.123 ;
    DESIGN testProperty2 STRING "as df" ;
END PROPERTYDEFINITIONS

DIEAREA ( 0 0 ) ( 10000 10000 ) ;

ROW row0 site1 100 200 S ;
ROW row1 site1 100 200 S DO 10 BY 1 ;
ROW row2 site1 100 200 S DO 10 BY 1 STEP 123 456 ;

TRACKS X 100 DO 1234 STEP 400 LAYER metal1 ;
TRACKS Y 100 DO 1234 STEP 400 LAYER metal1 metal2 ;
TRACKS X 100 DO 1234 STEP 400 MASK 1 LAYER metal3 ;
TRACKS X 100 DO 1234 STEP 400 MASK 1 SAMEMASK LAYER metal4 ;

COMPONENTS 3 ;
    - _1_ BUF_X2 ;
    - _2_ BUF_X2 ;
    - _3_ BUF_X2 ;
END COMPONENTS

BEGINEXT
    test 123 ;
ENDEXT

END DESIGN
"#;

    // Parse the DEF, format it and parse it again.

    let result = def_parser::read_def_chars(data.chars());
    assert!(result.is_ok());
    let def1 = result.unwrap();

    let mut sink = Vec::new();

    def_writer::write_def(&mut sink, &def1).unwrap();
    let data2 = String::from_utf8(sink).unwrap();

    print!("{}", &data2);

    let result2 = def_parser::read_def_chars(data2.chars());
    dbg!(&result2);
    assert!(result2.is_ok());
    let def2 = result2.unwrap();

    // Do some sanity checks.
    assert_eq!(def1.design_name, def2.design_name);
    assert_eq!(def1.technology, def2.technology);
    assert_eq!(def1.components.len(), def2.components.len());
    assert_eq!(def1.tracks.len(), def2.tracks.len());
    assert_eq!(def1.rows.len(), def2.rows.len());
}
