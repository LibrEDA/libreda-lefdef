#![cfg(test)]

// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use libreda_lefdef::lef_parser;
use std::fs::File;
use std::io::BufReader;

#[test]
fn test_read_lef_freepdk45() {
    let f = File::open("./tests/data/lef_examples/freepdk45/gscl45nm.lef").unwrap();
    let mut buf = BufReader::new(f);

    let result = lef_parser::read_lef_bytes(&mut buf);

    dbg!(&result);

    assert!(result.is_ok());
}

#[test]
fn test_read_lef_asap7() {
    let f = File::open("./tests/data/lef_examples/asap7/asap7_tech_4x_201209.lef").unwrap();
    let mut buf = BufReader::new(f);

    let result = lef_parser::read_lef_bytes(&mut buf);

    dbg!(&result);

    assert!(result.is_ok());
}

#[test]
fn test_read_lef_sky130() {
    let f = File::open("./tests/data/lef_examples/sky130/sky130_fd_sc_hd.tlef").unwrap();
    let mut buf = BufReader::new(f);

    let result = lef_parser::read_lef_bytes(&mut buf);

    dbg!(&result);

    assert!(result.is_ok());
}
