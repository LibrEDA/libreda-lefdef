<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# LEF/DEF parser and writer

LEF (Library Exchange Format) and DEF (Design Exchange Format) are industry standard file formats for describing 
standard cell libraries, technology data (LEF) and whole designs including layout and netlists (DEF).

This crate provides input and output functions for this formats including datastructures for their representation.

## Documentation

Documentation is generated from source with:
```sh
git clone [this repo]
cd [this repo]
cargo doc --open
```

Or alternatively find the documentation hosted [here](https://libreda.org/doc/libreda_lefdef/index.html).

## TODOs

* [ ] support for serde

## References

* LEF/DEF: http://www.ispd.cc/contests/18/lefdefref.pdf

## Licence

* The program code is licensed under the AGPLv3
* Some test data from FreePDK45 and Sky130 is licensed under Apache 2.0
