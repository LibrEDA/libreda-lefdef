#![cfg(test)]

// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use libreda_lefdef::def_parser;

#[test]
fn test_read_rows() {
    // Simple DEF file.
    let data = r#"
VERSION 5.7 ;
DIVIDERCHAR "|" ;
BUSBITCHARS "<>" ;
DESIGN test_design ;
UNITS DISTANCE MICRONS 2000 ;
TECHNOLOGY FreePDK45 ;

PROPERTYDEFINITIONS
    DESIGN testProperty REAL 0.123 ;
END PROPERTYDEFINITIONS

DIEAREA ( 0 0 ) ( 10000 10000 ) ;

ROW row0 site1 100 200 S ;
ROW row1 site1 100 200 S DO 10 BY 1 ;
ROW row2 site1 100 200 S DO 10 BY 1 STEP 123 456 ;

END DESIGN
"#;

    // Parse the DEF, format it and parse it again.

    let result = def_parser::read_def_chars(data.chars());
    assert!(result.is_ok());
    let def = result.unwrap();

    assert!(def.rows.contains_key("row0"));
    assert!(def.rows.contains_key("row1"));
    assert!(def.rows.contains_key("row2"));

    let row2 = &def.rows["row2"];
    assert_eq!(row2.site_name, "site1");
    assert_eq!(row2.orig, (100, 200));
    assert_eq!(row2.step_pattern.num_x, 10);
    assert_eq!(row2.step_pattern.num_y, 1);
    assert_eq!(row2.step_pattern.step, Some((123, 456)));
}

#[test]
fn test_read_tracks() {
    // Simple DEF file.
    let data = r#"
VERSION 5.7 ;
DIVIDERCHAR "|" ;
BUSBITCHARS "<>" ;
DESIGN test_design ;
UNITS DISTANCE MICRONS 2000 ;
TECHNOLOGY FreePDK45 ;

DIEAREA ( 0 0 ) ( 10000 10000 ) ;

TRACKS X 100 DO 1234 STEP 400 LAYER metal1 ;
TRACKS Y 100 DO 1234 STEP 400 LAYER metal1 metal2 ;
TRACKS X 100 DO 1234 STEP 400 MASK 1 LAYER metal3 ;
TRACKS X 100 DO 1234 STEP 400 MASK 1 SAMEMASK LAYER metal4 ;

END DESIGN
"#;

    // Parse the DEF, format it and parse it again.

    let result = def_parser::read_def_chars(data.chars());
    assert!(result.is_ok());
    let def = result.unwrap();

    assert_eq!(def.tracks.len(), 4);
    assert_eq!(def.tracks[3].is_horizontal, false);
    assert_eq!(def.tracks[3].start, 100);
    assert_eq!(def.tracks[3].num_tracks, 1234);
    assert_eq!(def.tracks[3].step, 400);
    assert_eq!(def.tracks[3].mask, Some((1, true)));
    assert_eq!(def.tracks[3].layers, vec!["metal4"]);
}
